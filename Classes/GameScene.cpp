#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "Consts.h"
#include "Screen.h"
#include "Bird.h"
#include "PhysicController.h"
#include "Edge.h"
#include "ScoreBar.h"
#include "Score.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    
    auto layer = GameScene::create();
    layer->sceneWorld = scene->getPhysicsWorld();

    scene->addChild(layer);

    return scene;
}
bool GameScene::init()
{
    if(!Scene::init())
    {
        return false;
    }
    
    auto backgroundSprite = Sprite::create(textures::background);
    backgroundSprite->setPosition(screen::getCenter());
    
    addChild(backgroundSprite);

    ground.spawn(this);

    const int pipeSchedule = 2;
    schedule(schedule_selector(GameScene::spawnPipe), pipeSchedule);

    auto edge = Edge::create();
    addChild(edge);

    auto bird = Bird::create(textures::bird);
    addChild(bird);

    auto score = Score::create();
    addChild(score);

    auto scoreBar = ScoreBar::create(*score);
    addChild(scoreBar, 10);

    auto physicController = PhysicController::create();
    addChild(physicController);
    
    return true;
}

void GameScene::spawnPipe(float dt)
{
    pipe.spawn(this);
}
