#pragma once

#include "cocos2d.h"
#include "Ground.h"
#include "Pipe.h"

class GameScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    CREATE_FUNC(GameScene);
private:
    void spawnPipe(float dt);
    
    Pipe pipe;
    
    cocos2d::PhysicsWorld *sceneWorld = nullptr;
    
    Ground ground;
};
