#pragma once

#include "cocos2d.h"
#include "ScoreObserver.h"

class Score :  public cocos2d::Node, public ScoreSubject
{
public:
    static Score *create();

    void increaseScore();

    void addEventListener();

    ~Score();
private:
    int score = 0;
    
    cocos2d::EventListenerCustom* listener = nullptr;
};
