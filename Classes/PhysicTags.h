#pragma once

#include <string>

namespace physic_tags {

const std::string pipe = "pipe";

const std::string point = "point";

const std::string bird = "bird";

const std::string edge = "edge";

const std::string ground = "ground";

}