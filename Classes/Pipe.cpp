#include "Pipe.h"
#include "Consts.h"
#include "PhysicTags.h"

USING_NS_CC;

const int pipeSpace = 150;

Pipe::Pipe()
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
}
void Pipe::spawn(cocos2d::Scene *scene)
{
    auto topPipe = Sprite::create(textures::pipe);
    auto bottomPipe = Sprite::create(textures::pipe);
    topPipe->setRotation(180);
 
    setPhysic(topPipe);
    setPhysic(bottomPipe);
    setRandomSpawnPosition(topPipe, bottomPipe);
    
    auto point = createPoint(topPipe);
    scene->addChild(topPipe);
    scene->addChild(bottomPipe);
    scene->addChild(point);    
   
    const float movingTime = 4.5f;
    auto moveForward = MoveBy::create(movingTime, Vec2(-visibleSize.width * 1.5, 0));
    auto remove = RemoveSelf::create();
    auto moveSequence = Sequence::create(moveForward, remove, nullptr); 
    topPipe->runAction(moveSequence);
    bottomPipe->runAction(moveSequence->clone());
    point->runAction(moveSequence->clone());
}

void Pipe::setPhysic(cocos2d::Sprite *sprite)
{
    auto physicBody = PhysicsBody::createBox(sprite->getContentSize());
    physicBody->setDynamic(false);
    physicBody->setContactTestBitmask(0xFFFFFFFF);
    physicBody->setName(physic_tags::pipe);
    sprite->setPhysicsBody(physicBody);
}

void Pipe::setRandomSpawnPosition(cocos2d::Sprite *top, cocos2d::Sprite *bot)
{
    auto random = CCRANDOM_0_1();
    float randomPosition = std::min(std::max(0.3f, random), 0.65f) * visibleSize.height;
    top->setPosition(Vec2(visibleSize.width + top->getContentSize().width,
                          randomPosition + top->getContentSize().height));
    bot->setPosition(Vec2(visibleSize.width + bot->getContentSize().width,
                          randomPosition - pipeSpace));
}

Node* Pipe::createPoint(Sprite* toppipe)
{
    auto point = Node::create();
    auto pointBody = PhysicsBody::createBox(Size(1, pipeSpace));
    
    pointBody->setDynamic(false);
    pointBody->setContactTestBitmask(0xFFFFFFFF);
    pointBody->setName(physic_tags::point);
    
    point->setPhysicsBody(pointBody);
    point->setPosition(toppipe->getPositionX(),
                       toppipe->getPositionY() - toppipe->getContentSize().height / 2 - pipeSpace / 2);
    return point;
}
