#include "ScoreBar.h"
#include "Consts.h"
#include "Screen.h"

ScoreBar *ScoreBar::create(ScoreSubject &subject)
{
    auto self = new ScoreBar();
    self->autorelease();
    
    subject.addObserver(*self);
    self->subject = &subject;
  
    std::string text = "0";
    int fontSize = 50;
    auto label = self->label = Label::createWithTTF(text, fonts::arial, fontSize);
    label->setAnchorPoint(anchor::right);
    self->addChild(label);
    
    self->setPosition(screen::getUpRightCorner());

    return self;
}

ScoreBar::~ScoreBar()
{
    subject->removeObserver(*this);
}

void ScoreBar::onNotify(int score)
{
    label->setString(std::to_string(score));
}
