#pragma once

#include <string>

namespace textures{

const std::string background = "Background.png";

const std::string ground = "Ground.png";

const std::string pipe = "Pipe.png";

const std::string bird = "Bird.png";

}

namespace fonts{

const std::string arial = "fonts/arial.ttf";

}

namespace events{

const char *const pipeIsPassed = "pipeIsPassed";

}

