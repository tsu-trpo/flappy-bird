#pragma once

#include "cocos2d.h"

class Bird : public cocos2d::Sprite
{
public:
    static Bird* create(const std::string &sprite);

    void update(float dt) override;

    void fly();

    void stopFlying(float dt);

    cocos2d::EventKeyboard::KeyCode onKeyboardPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
private:
    const float movingDelta = 0.6 * cocos2d::Director::getInstance()->getVisibleSize().height;
    
    bool isFalling = true;
};
