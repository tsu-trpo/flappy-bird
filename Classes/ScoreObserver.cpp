#include "ScoreObserver.h"
#include <algorithm>

void ScoreSubject::addObserver(ScoreObserver &observer)
{
    observers.push_back(&observer);
}

void ScoreSubject::removeObserver(ScoreObserver &observer)
{
    auto found = std::find(observers.begin(), observers.end(), &observer);
    if (found != observers.end()) {
        observers.erase(found);
    }
}

void ScoreSubject::notify(int score)
{
    for (auto &o : observers) {
        o->onNotify(score);
    }
}
