#include "PhysicController.h"
#include "ContactHelper.h"
#include "GameScene.h"
#include "PhysicTags.h"
#include "Bird.h"
#include "Consts.h"

PhysicController *PhysicController::create()
{
    auto self = new PhysicController();
    self->autorelease();

    self->listener = cocos2d::EventListenerPhysicsContact::create();
    self->listener->onContactBegin = CC_CALLBACK_1(PhysicController::handler, self);

    auto dispatcher = self->getEventDispatcher();
    dispatcher->addEventListenerWithSceneGraphPriority(self->listener, self);

    return self;
}

bool PhysicController::handler(cocos2d::PhysicsContact &contact)
{
    ContactHelper helper{contact, physic_tags::bird};
    if(!helper.wasContacted())
    {
        return false;
    }

    cocos2d::PhysicsShape *other = helper.getOther();
    if(isTagEqualTo(other, physic_tags::pipe) ||
       isTagEqualTo(other, physic_tags::edge) ||
       isTagEqualTo(other, physic_tags::ground))
    {
        cocos2d::Director::getInstance()->replaceScene(GameScene::createScene());
        return true;
    }
    else if(isTagEqualTo(other, physic_tags::point))
    {
        getEventDispatcher()->dispatchCustomEvent(events::pipeIsPassed);
    }
    return false;
}
