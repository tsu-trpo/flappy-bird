#include <string>
#include "cocos2d.h"

inline bool isTagEqualTo(const cocos2d::PhysicsShape *shape, const std::string &tag)
{
    return shape->getBody()->getName() == tag;
}

class ContactHelper {
public:
    ContactHelper(cocos2d::PhysicsContact &contact, const std::string &mainTag);

    bool wasContacted();

    cocos2d::PhysicsShape *getMain();
    cocos2d::PhysicsShape *getOther();

private:
    cocos2d::PhysicsShape *_main = nullptr;
    cocos2d::PhysicsShape *_other = nullptr;
};
