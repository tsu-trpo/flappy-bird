#include "Bird.h"
#include "Consts.h"
#include "Screen.h"
#include "PhysicTags.h"

USING_NS_CC;

Bird* Bird::create(const std::string &sprite)
{
    auto self = new Bird();
    self->autorelease();
    self->scheduleUpdate();

    self->initWithFile(sprite);

    self->setPosition(screen::getCenter());
    auto physicBody = cocos2d::PhysicsBody::createCircle(self->getContentSize().width / 2);
    physicBody->setDynamic(false);
    physicBody->setContactTestBitmask(0xFFFFFFFF);
    physicBody->setName(physic_tags::bird);
    self->setPhysicsBody(physicBody);
    
    auto listener = cocos2d::EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(Bird::onKeyboardPressed, self);
    self->_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, self);

    return self;
}

void Bird::update(float dt)
{
    float inverter = (isFalling ? -1.f : +1.f);
    setPositionY(getPositionY() + movingDelta * inverter * dt);
    setRotation(-45 * inverter);
}

void Bird::stopFlying(float dt)
{
    isFalling = true;
}

void Bird::fly()
{
    isFalling = false;
}

cocos2d::EventKeyboard::KeyCode Bird::onKeyboardPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_SPACE)
    {
        const float flyTime = 0.3f;
        fly();
        scheduleOnce(schedule_selector(Bird::stopFlying), flyTime);
    }
    return keyCode;
}
