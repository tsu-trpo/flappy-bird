#pragma once

#include "cocos2d.h"

USING_NS_CC;

namespace Utils{

inline Vec2 getSpriteCenter(Sprite* source)
{
    return Vec2{source->getContentSize().width / 2,
                source->getContentSize().height / 2};

}

}
