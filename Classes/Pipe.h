#pragma once

#include "cocos2d.h"

class Pipe
{
public:
    Pipe();
   
    void spawn(cocos2d::Scene *scene);
private:
    cocos2d::Size visibleSize;
   
    cocos2d::Vec2 origin;

    void setPhysic(cocos2d::Sprite *srite);

    void setRandomSpawnPosition(cocos2d::Sprite *top, cocos2d::Sprite *bot);

    cocos2d::Node* createPoint(cocos2d::Sprite *toppipe);
};

