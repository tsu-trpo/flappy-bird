#include "Score.h"
#include "Consts.h"

USING_NS_CC;

Score *Score::create()
{
    auto self = new Score();
    self->autorelease();
    self->addEventListener();
    return self;
}


void Score::addEventListener()
{
    listener = getEventDispatcher()->addCustomEventListener(events::pipeIsPassed, [&](EventCustom *event) { increaseScore(); });
}

Score::~Score()
{
    getEventDispatcher()->removeEventListener(listener);
}

void Score::increaseScore()
{
    score++;
    notify(score);
}
