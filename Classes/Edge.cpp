#include "Edge.h"
#include "PhysicTags.h"
#include "Screen.h"

USING_NS_CC;

Edge* Edge::create()
{
    auto self = new Edge();
    self->autorelease();

    auto physicBody = PhysicsBody::createEdgeBox(Director::getInstance()->getVisibleSize(), PHYSICSBODY_MATERIAL_DEFAULT, 1);
    physicBody->setDynamic(false);
    physicBody->setContactTestBitmask(0xFFFFFFFF);
    physicBody->setName(physic_tags::edge);

    self->setPhysicsBody(physicBody);
    self->setPosition(screen::getCenter());
    return self;
}
