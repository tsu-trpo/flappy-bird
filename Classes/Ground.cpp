#include "Ground.h"
#include "Consts.h"
#include "Utils.h"
#include "PhysicTags.h"

USING_NS_CC;

Ground::Ground()
{
    sprite = Sprite::create(textures::ground);
    auto groundBody = PhysicsBody::createBox(sprite->getContentSize());
    groundBody->setDynamic(false);
    groundBody->setContactTestBitmask(0xFFFFFFFF);
    groundBody->setName(physic_tags::ground);
    sprite->setPhysicsBody(groundBody);
    sprite->setPosition(Utils::getSpriteCenter(sprite));
}

void Ground::spawn(cocos2d::Scene *layer)
{
    layer->addChild(sprite, 10);
    const float time = 0.5f;
    const float step = -48.f;
    auto moveForward = MoveBy::create(time, Vec2(step, 0));
    auto moveBack = MoveTo::create(0, Utils::getSpriteCenter(sprite));
    sprite->runAction(RepeatForever::create(Sequence::create(moveForward, moveBack, nullptr)));
}
