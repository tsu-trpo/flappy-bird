#pragma once

#include "cocos2d.h"

USING_NS_CC;

namespace screen{

inline Vec2 getCenter()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return Vec2{origin.x + visibleSize.width / 2.0f,
                origin.y + visibleSize.height / 2.0f};
}

inline Vec2 getUpRightCorner()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return origin + visibleSize;
}

}

namespace anchor{

const Vec2 right = {1.f, 1.f};

}
