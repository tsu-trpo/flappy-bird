#pragma once

#include "cocos2d.h"
#include "ScoreObserver.h"

class ScoreBar : public cocos2d::Node, public ScoreObserver
{
public:
    static ScoreBar *create(ScoreSubject &subject);

    void onNotify(int score) override;

    ~ScoreBar();
private:
    cocos2d::Label *label = nullptr;

    ScoreSubject *subject = nullptr;
};
