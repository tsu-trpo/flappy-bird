#pragma once

#include "cocos2d.h"

class Ground
{
public:
    Ground();

    void spawn(cocos2d::Scene *layer);
private:
    cocos2d::Sprite* sprite;
};
