#pragma once

#include <memory>
#include <vector>

class ScoreObserver
{
public:
    virtual ~ScoreObserver() = default;

    virtual void onNotify(int score) = 0;
};

class ScoreSubject
{
public:
    void addObserver(ScoreObserver &observer);
    
    void removeObserver(ScoreObserver &observer);
    
    void notify(int health);

private:
    std::vector<ScoreObserver *> observers;
};
